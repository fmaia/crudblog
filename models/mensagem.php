<?php
	// Arquivo mensagem.php

	class Mensagem{
		private $id;
		private $autor;
		private $conteudo;

		public function __construct($id, $autor, 
			$conteudo){
			$this->id = $id;
			$this->autor = $autor;
			$this->conteudo = $conteudo;
		}
		public function getId(){
			return $this->id;
		}
		public function setId($id){
			$this->id = $id;
		}
		public function getAutor(){
			return $this->autor;
		}
		public function setAutor($autor){
			$this->autor = $autor;
		}
		public function getConteudo(){
			return $this->conteudo;
		}
		public function setConteudo($conteudo){
			$this->conteudo = $conteudo;
		}
		public function getResumo(){
			return "[".$this->autor."] ".$this->conteudo;
		}
		public static function all(){
			$bd = Db::getInstance();
			$list = [];
			$req = $bd->query('SELECT * FROM mensagem');
			foreach ($req->fetchAll() as $mensagem) {
				$list[] = new Mensagem(
									$mensagem['id'],
									$mensagem['autor'],
									$mensagem['conteudo']); 
			}
			return $list;
		}

		public static function find($id){
			$bd = Db::getInstance();
			$id = intval($id);
		    $req = $bd->prepare('SELECT * FROM mensagem 
		    					 WHERE id = :id'); 
		    $req->execute(array('id' => $id));
		    $mensagem = $req->fetch();
		    return new Mensagem($mensagem['id'], 
		    				$mensagem['autor'], 
		    				$mensagem['conteudo']);
		}



		public static function insert($mensagem){
			$bd = Db::getInstance();
			$req = $bd->prepare("INSERT INTO 
							mensagem(autor, conteudo)
							VALUES(:autor, :conteudo)");
			$req->execute(
				array("autor" => $mensagem->autor,
					  "conteudo" => $mensagem->conteudo ));
		}

		public static function delete($id){
			$bd = Db::getInstance();
			$id = intval($id);
			$req = $bd->prepare("DELETE FROM mensagem
								WHERE id=:id");
			$req->execute(array("id"=>$id));
		}

		public static function update($mensagem){
			$bd = Db::getInstance();
			$req = $bd->prepare("UPDATE mensagem
								SET autor = :autor, 
									conteudo = :conteudo
								WHERE id = :id");
			$req->execute(array(
						"autor" => $mensagem->autor,
						"conteudo" => $mensagem->conteudo,
						"id" => $mensagem->id));
		}


	}
?>