<?php
	// Arquivo comentário.php

	class Comentario{
		private $id;
		private $mensagem_id;
		private $autor;
		private $conteudo;

		public function __construct($id, $mensagem_id, $autor, 
			$conteudo){
			$this->id = $id;
			$this->mensagem_id = $mensagem_id;
			$this->autor = $autor;
			$this->conteudo = $conteudo;
		}
		public function getId(){
			return $this->id;
		}
		public function setId($id){
			$this->id = $id;
		}
		public function getMensagemId(){
			return $this->mensagem_id;
		}
		public function setMensagemId($mensagem_id){
			$this->mensagem_id = $mensagem_id;
		}
		public function getAutor(){
			return $this->autor;
		}
		public function setAutor($autor){
			$this->autor = $autor;
		}
		public function getConteudo(){
			return $this->conteudo;
		}
		public function setConteudo($conteudo){
			$this->conteudo = $conteudo;
		}
		public static function all(){
			$bd = Db::getInstance();
			$list = [];
			$req = $bd->query('SELECT * FROM comentario');
			foreach ($req->fetchAll() as $comentario) {
				$list[] = new Comentario(	
							$comentario['id'],
							$comentario['mensagem_id'],
							$comentario['autor'],
							$comentario['conteudo']); 
			}
			return $list;
		}

		public static function find($id){
			$bd = Db::getInstance();
			$id = intval($id);
		    $req = $bd->prepare('SELECT * FROM comentario 
		    					 WHERE id = :id'); 
		    $req->execute(array('id' => $id));
		    $comentario = $req->fetch();
		    return new comentario(
		    				$comentario['id'],
		    				$comentario['mensagem_id'], 
		    				$comentario['autor'], 
		    				$comentario['conteudo']);
		}

		public static function findFromMensagem(
									$mensagem_id){
			$bd = Db::getInstance();
			$list = [];
			$mensagem_id = intval($mensagem_id);
		    $req = $bd->prepare('SELECT * FROM comentario 
		    					 WHERE mensagem_id = :mensagem_id'); 
		    $req->execute(
		    	array('mensagem_id' => $mensagem_id));
		    foreach ($req->fetchAll() as $comentario) {
				$list[] = new Comentario(	
							$comentario['id'],
							$comentario['mensagem_id'],
							$comentario['autor'],
							$comentario['conteudo']); 
			}
			return $list;
		}



		public static function insert($comentario){
			$bd = Db::getInstance();
			$req = $bd->prepare("INSERT INTO 
					comentario(mensagem_id, autor, conteudo)
					VALUES(:mensagem_id, :autor, :conteudo)");
			$req->execute(
			  array("mensagem_id" => $comentario->mensagem_id,
			  		"autor" => $comentario->autor,
			 	    "conteudo" => $comentario->conteudo ));
		}

		public static function delete($id){
			$bd = Db::getInstance();
			$id = intval($id);
			$req = $bd->prepare("DELETE FROM comentario
								WHERE id=:id");
			$req->execute(array("id"=>$id));
		}

		public static function update($comentario){
			$bd = Db::getInstance();
			$req = $bd->prepare("UPDATE comentario
								SET mensagem_id = :mensagem_id,
									autor = :autor, 
									conteudo = :conteudo
								WHERE id = :id");
			$req->execute(array(
					"mensagem_id" => $comentario->mensagem_id,
					"autor" => $comentario->autor,
					"conteudo" => $comentario->conteudo,
					"id" => $comentario->id));
		}


	}
?>