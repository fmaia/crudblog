# Informações básicas

Este é um simples exemplo de CRUD.

Possui 2 modelos de entidades:

* Mensagem: que armazena as mensagens e possui como atributos o id, autor e conteúdo.
* Comentario: que armazena as respostas a uma mensagem e possui como atributos o id da mensagem respondida, o autor e o conteúdo da resposta.

Esses 2 modelos podem ser vistos na pasta models.

Os controladores para esses modelos, ficam dentro da pasta controllers.

As visualizações dos modelos ficam na pasta views, cada um dentro de sua respectiva pasta.

## Os models

Dentro da pasta models temos os arquivos mensagem.php, e comentario.php.

Esses arquivos possuem as classes usadas para representar respectivamente as mensagens e os comentários. 

Cada classe possui seus atributos, contrutores e metodos get e set. Tudo semelhante ao que já é conhecido de POO, sendo que aqui está implementado em PHP.

Dentro das classes também estão implementados os metodos de acesso ao banco de dados. Os principais são:

* all: consulta a tabela e retorna todos os seus registros.
* find: consulta a tabela e retorna um registro específico, de acordo com o id informado.
* insert: insere novos registros na tabela.
* delete: remove um registro específico da tabela, de acordo com o id informado.
* update: atualiza um registro específico na tabela.

Além desses, dado o relacionamento entre mensagem e comentario, dentro da classe comentario existe um outro método: 

* findFromMensagem: esse método percorre a tabela comentário e retorna todos os comenstários de uma mensagem específica, de acordo com um id de menságem informádo.


## Os controllers 

Na pasta controllers, temos uma classe para cada modelo. Estão nos arquivos comentario_controller.php, e mensagem_controller.php.

Essas classes possuem um método para cada ação do CRUD:

* insert: é utilizado para controlar a inserção de novos registros (o C do CRUD).
* show: é utilizado para controlar a exibição dos registros (o R do CRUD).
* updade: é utilizado para controlar a modificação dos registros (o U do CRUD).
* delete: é utilizado para controlar a remoção dos registros (o D do CRUD).

### A exibição

O método show é o mais simples, ele apenas consulta todos os registros no banco usando o método All da classe modelo.

Em seguida são utilizados os arquivos de exibição (views) para mostrar esse conteúdo.

### A inserção

O método insert deve receber os dados do novo registro através dos argumentos do método \_POST do HTTP. 

Esses parâmetros são utilizados para criar um novo registro e em seguinda fazer sua inserção no banco de dados.

Em seguida é feito a exibição assim como no método show.

### A remoção

O método delete deve receber o id do registro para ser deletado através dos argumentos do método \_GET do HTTP. 

Esses o registro é deletado utilizando o método da classe modelo.

Em seguida é feito a exibição assim como no método show.

### A atualização

O método possui dois casos:

* o método está recebendo os dados de um registro que está sendo atualizado, nesse casos os valores estão chegando através do método \_POST do HTTP. Esse caso é semelhante a um inserte, sendo que é chamado um updade.
* o método não está recebendo esses dados, nesse caso é necessário exibir uma tela diferente para o usuário poder alterar os valores de um registro existente. Nesse caso ele apenas faz uma consulta utilizando o método find e em seguinda exibe a tela correspondente.

A diferenciação é feita verificando se o id do registro está chegando via \_POST. Caso esteja, temos o primeiro caso, do contrário temos o segundo caso.

## As views
A pasta views contém todos os arquivos com código HTML e CSS utilizados para definir a exibição dos conteúdos.

O arquivo layout.php contém o cabeçalho e rodapé de todas das páginas. Nesse arquivo, tudo que for colocado entre as linhas 46 e 51 será exibido antes do conteúdo, tudo que for colocado a partir da linha 53 ficará após o conteúdo. Esse conteúdo será definido durante a execução pelos controladores. Na linha 52 o arquivo routes.php é chamado pois ele fica encarredado escolher o controlador e a ação correta para cada situação. Além disso nesse arquivo estão todos os códigos CSS necessários para o sistema.

Dentro da pasta views também se encontram uma pasta para cada controlador, uma para mensagem e outra para comentario. Dentro dessas pastas encontramos os seguintes arquivos: 

* insert: contém o formulário para enviar os dados de um novo registro. O envio é feito através do método \_POST do HTTP. No caso do insert do comentario, é necessário código PHP para preencher as opções do elemento <select\> que é uma caixa de seleção. Esse preenchimento ocorre de acordo com uma consulta no banco de dados que foi requisitada pelo comentario_controller.
* show: contém uma tabela para a exibição de todos os registros. Para preenchimento dessa tabela é utilizado código PHP. Para a coluna ação existem botões para as ações de atualizar e deletar o registro; quando esses botões são acionados, são passados o controlador, a ação e o id do registro através do método \_GET do HTTP.
* updade: também contém um formulário, cona diferença que seus valores já devem ser preenchidos com os valores anteriores do registro que está sendo modificado. O preenchimento dos valores é feito utilizando código PHP. No caso do comentario é necessário marcar também qual opção já está selecionada dentro do seu <select\>. 

## Outros arquivos

### connection

O arquivo connection.php contém o código necessário para realizar a conexão com o SGBD. No caso dessa aplicação está sendo utilizado MySql. 

Na linha são informado parâmetros que também devem variar de acordo com a aplicação, como o endereço do SGBD, o nome do banco, o usuário e a senha de acesso. 

Um backup para a criação das tabelas pode ser visto no arquivo tabelas.sql.


### index

O arquivo index.php contém código PHP que realiza as seguinte ações:

1- Importa os códigos para a conexão com banco de dados.
2- Verifica se algum controlador e ação estão sendo informados pelos \_GET do HTTP. Caso esteja, armazena qual controlador e qual ação devem ser executados. Caso não esteja chegado nada, utiliza o controlador mensagem e a ação show como padrão.
3- Chama o layout.php que é resposável por toda a exebição.

### routes

O arquivo routes, PHP observa qual ação em qual controller deve ser executado e em seguinda executa.

Observe que nesse arquivo, temos um case para cada controller. Quando mais controllers forem adicionados, será necessário inserir os seus cases correspondentes.


