<!DOCTYPE html>
<html>
<head>
	<title>Exemplo de CRUD</title>
	<meta charset="utf-8">

	<style type="text/css">
		.button {
		  background-color: #001f3f; /* Green */
		  border: none;
		  color: white;
		  padding: 15px 32px;
		  text-align: center;
		  text-decoration: none;
		  display: inline-block;
		  font-size: 16px;
		}

		#table {
		  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		  border-collapse: collapse;
		  width: 100%;
		}

		#table td, #table th {
		  border: 1px solid #ddd;
		  padding: 8px;
		}

		#table tr:nth-child(even){background-color: #f2f2f2;}

		#table tr:hover {background-color: #ddd;}

		#table th {
		  padding-top: 12px;
		  padding-bottom: 12px;
		  text-align: left;
		  background-color: #001f3f;
		  color: white;
		}
	</style>

</head>
<body>

<!-- Logo abaixo os botões do menu, fazer um para cada entidade-->
<a href="index.php?controller=mensagem&action=show" 
	class="button">Mensagens</a>
<a href="index.php?controller=comentario&action=show" 
	class="button">Comentários</a>

<?php require_once("routes.php"); ?>

<br><br>
<p>Esse sistema foi criado como um simples 
	exemplo de crud.</p>
</body>
</html>