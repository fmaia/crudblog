<?php
	//Arquivo: comentario_controller.php
	
	class ComentarioController{

		public function show(){
			$comentarios = Comentario::all();
			$mensagens = Mensagem::all();
			require_once("views/comentario/insert.php");
			require_once("views/comentario/show.php");
		}

		public function insert(){
			Comentario::insert(new Comentario(0,
						$_POST["mensagem_id"],
						$_POST["autor"],
						$_POST["conteudo"]
						));
			$comentarios = Comentario::all();
			$mensagens = Mensagem::all();
			require_once("views/comentario/insert.php");
			require_once("views/comentario/show.php");
		}

		public function delete(){
			Comentario::delete($_GET["id"]);
			$comentarios = Comentario::all();
			$mensagens = Mensagem::all();
			require_once("views/comentario/insert.php");
			require_once("views/comentario/show.php");
		}

		public function update(){
			if(isset($_POST["id"])){
				Comentario::update(
							new Comentario(
									$_POST["id"],
									$_POST["mensagem_id"],
									$_POST["autor"],
									$_POST["conteudo"]));
				$comentarios = Comentario::all();
				$mensagens = Mensagem::all();
				require_once("views/comentario/insert.php");
				require_once("views/comentario/show.php");
			} else {
				$comentario= Comentario::find($_GET["id"]);
				$mensagens = Mensagem::all();
				require_once("views/comentario/update.php");
			}
		}
	}
?>