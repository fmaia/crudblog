<?php
	//Arquivo: mensagem_controller.php
	
	class MensagemController{

		public function show(){
			$mensagens = Mensagem::all();
			require_once("views/mensagem/insert.php");
			require_once("views/mensagem/show.php");
		}

		public function insert(){
			Mensagem::insert(new Mensagem(0,
						$_POST["autor"],
						$_POST["conteudo"]
						));
			$mensagens = Mensagem::all();
			require_once("views/mensagem/insert.php");
			require_once("views/mensagem/show.php");
		}

		public function delete(){
			Mensagem::delete($_GET["id"]);
			$mensagens = Mensagem::all();
			require_once("views/mensagem/insert.php");
			require_once("views/mensagem/show.php");
		}
		public function update(){
			if(isset($_POST["id"])){
				Mensagem::update(new Mensagem($_POST["id"],
									  $_POST["autor"],
									 $_POST["conteudo"]));
				$mensagens = Mensagem::all();
				require_once("views/mensagem/insert.php");
				require_once("views/mensagem/show.php");
			} else {
				$mensagem= Mensagem::find($_GET["id"]);
				require_once("views/mensagem/update.php");
			}
		}
	}
?>